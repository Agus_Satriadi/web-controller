<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return view('\index', ['nama'=> 'Agus Satriadi']);
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('kontak');
    }
}
